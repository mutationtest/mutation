25/03/2021

Para poder correr el proyecto de forma local, se debe de ejecutar el archivo Mutacion.sql en 
SQL server. Una vez creada la base y la tabla, se tiene que ir al archivo appsettings.json y cambiar la 
cadena de conexión apuntando a la base loca.

Las peticiones para el servicio en azure son las siguientes:
https://webapimutacion.azurewebsites.net/api/stats
https://webapimutacion.azurewebsites.net/api/mutation

"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGATAG", "CCCCTA", "TCACTG"] Horizontal
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGATAG", "CCGTTA", "TCACTG"] Vertical
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAATG", "CCGCTA", "TCACTG"] Oblicua L->R
"dna":["AGCGTA", "CGTGAC", "TGTATT", "GTAAGA", "ATCGCC", "GTCACT"] Oblicua R->L
"dna":["TCACTG", "CAGTGC", "TTATGT", "TCCCCA", "AGATGG", "ATGCGA"] Horizontal
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGATGG", "CCGTTA", "TCACTG"] Vertical
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAATG", "CCGCTA", "TCACTG"] Oblicua L->R
"dna":["AGCGTA", "CGTGAC", "TGTATT", "GTAAGA", "ATCGCC", "GTCACT"] Oblicua L->R
"dna":["TTGCGA", "CAGTGC", "TTATGT", "AGATTG", "CCGCAA", "TCACTG"] Sin

