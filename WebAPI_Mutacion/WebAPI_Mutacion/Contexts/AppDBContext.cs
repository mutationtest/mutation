﻿using Microsoft.EntityFrameworkCore;
using WebAPI_Mutacion.Modelos;

namespace WebAPI_Mutacion.Contexts
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        { }

        public DbSet<Mutacion> Mutacion { get; set; }
    }
}
