USE [master]
GO
/****** Object:  Database [proyectomutaciones-database]    Script Date: 25/03/2021 12:01:23 p. m. ******/
CREATE DATABASE [proyectomutaciones-database]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [proyectomutaciones-database].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [proyectomutaciones-database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ARITHABORT OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [proyectomutaciones-database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [proyectomutaciones-database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [proyectomutaciones-database] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [proyectomutaciones-database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET  MULTI_USER 
GO
ALTER DATABASE [proyectomutaciones-database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [proyectomutaciones-database] SET ENCRYPTION ON
GO
ALTER DATABASE [proyectomutaciones-database] SET QUERY_STORE = ON
GO
ALTER DATABASE [proyectomutaciones-database] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 7), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 10, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [proyectomutaciones-database]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 8;
GO
USE [proyectomutaciones-database]
GO
/****** Object:  Table [dbo].[Mutacion]    Script Date: 25/03/2021 12:01:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mutacion](
	[IdMutaciones] [int] IDENTITY(1,1) NOT NULL,
	[CadenaDNA] [varchar](max) NOT NULL,
	[TieneMutacion] [bit] NOT NULL,
 CONSTRAINT [PK_tblMutaciones] PRIMARY KEY CLUSTERED 
(
	[IdMutaciones] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [proyectomutaciones-database] SET  READ_WRITE 
GO
