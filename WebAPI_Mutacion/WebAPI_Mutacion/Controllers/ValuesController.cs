﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using WebAPI_Mutacion.Contexts;
using WebAPI_Mutacion.Modelos;

namespace WebAPI_Mutacion.Controllers
{
    [Route("api")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly AppDBContext context;

        public ValuesController(AppDBContext context)
        {
            this.context = context;
        }

        [HttpGet("stats")]
        public ActionResult<string> Get()
        {
            Dictionary<string, float> dcDatos = new Dictionary<string, float>();
            var ResultCount = from M in context.Mutacion
                              let TypeMutation = M.TieneMutacion ? "count_mutations" : "count_no_mutation"
                              group M by TypeMutation into TP
                              select new { Type = TP.Key, Count = TP.Count() };

            foreach (var Count in ResultCount)
                dcDatos.Add(Count.Type, Count.Count);

            dcDatos.TryGetValue("count_mutations", out var count_mutations);
            dcDatos.TryGetValue("count_no_mutation", out var count_no_mutation);
            dcDatos.Add("ratio", ((float)count_mutations/(float)count_no_mutation));
            return JsonConvert.SerializeObject(dcDatos); ;
        }

        // POST api/mutation
        [HttpPost("mutation")]
        public IActionResult Mutacion(DNAC M)
        {
            if (M.DNA.Length == 0)
                return StatusCode(403, "Status 403 - Sin Información");
            else
            {
                char[,] DNA = new char[M.DNA.Length, M.DNA.Length];
                char[] Letters = new char[M.DNA.Length];
                int i = 0, j = 0, Count = 1;
                Mutacion Mt;

                // El primer paso es llenar el Arreglo bidimensional, al mismo tiempo validar si existen letras repetidas.
                // Si se cumple, ya no es necesario seguir llenando y manda un Status 200, en caso contrario, termina de llenar la matriz.
                foreach (string HW in M.DNA)
                {
                    Letters = HW.ToCharArray();
                    for (i = 0; i <= Letters.Length - 1; i++)
                    {
                        if ((i > 0) && (Letters[i] == Letters[i - 1]))
                            Count++;
                        else
                            Count = 1;

                        if (Count == 4)
                        {
                            Mt = new Mutacion { CadenaDNA = string.Join(" ", M.DNA), TieneMutacion = true };
                            context.Mutacion.Add(Mt);
                            context.SaveChanges();
                            return StatusCode(200, "Status 200 - Con Mutación");
                        }
                        else
                            DNA[j,i] =  Letters[i];
                    }
                    j++;
                }

                // Se empieza a recorrer la matriz de forma vertical, si encuentra una secuencia, se detiene y devuelve Status 400
                for (i = 0; i <= DNA.GetLength(0) - 1; i++)
                {
                    for (j = 0; j <= DNA.GetLength(0) - 1; j++)
                    {
                        if ((j > 0) && (DNA[j,i] == DNA[j - 1,i]))
                            Count++;
                        else
                            Count = 1;

                        if (Count == 4)
                        {
                            Mt = new Mutacion { CadenaDNA = string.Join(" ", M.DNA), TieneMutacion = true };
                            context.Mutacion.Add(Mt);
                            context.SaveChanges();
                            return StatusCode(200, "Status 200 - Con Mutación");
                        }
                            
                    }
                }

                // Se empieza a validar de forma oblicua, de izquierda a derecha, como la instrucción son una matriz de NXN,
                // no puede haber combinaciones con menos de 6 letras. Las condiciones se cumplen cuando los indices i y j son iguales
                for (i = 0; i <= DNA.GetLength(0) - 1; i++)
                {
                    if ((i > 0) && (DNA[i, i] == DNA[i - 1, i - 1]))
                        Count++;
                    else
                        Count = 1;

                    if (Count == 4)
                    {
                        Mt = new Mutacion { CadenaDNA = string.Join(" ", M.DNA), TieneMutacion = true };
                        context.Mutacion.Add(Mt);
                        context.SaveChanges();
                        return StatusCode(200, "Status 200 - Con Mutación");
                    } 
                }

                // Se empieza a validar de forma oblicua, de derecha a izquierda, como la instrucción son una matriz de NXN,
                // no puede haber combinaciones con menos de 6 letras. Las condiciones se cumplen cuando los indices i y j son complementarios
                for (i = 0; i <= DNA.GetLength(0) - 1; i++)
                {
                    if ((i > 0) && (DNA[i, (DNA.GetLength(0) - 1) - i] == DNA[i - 1, (DNA.GetLength(0) - i)]))
                        Count++;
                    else
                        Count = 1;

                    if (Count == 4)
                    {
                        Mt = new Mutacion { CadenaDNA = string.Join(" ", M.DNA), TieneMutacion = true };
                        context.Mutacion.Add(Mt);
                        context.SaveChanges();
                        return StatusCode(200, "Status 200 - Con Mutación");
                    }
                }

                Mt = new Mutacion { CadenaDNA = string.Join(" ", M.DNA), TieneMutacion = false };
                context.Mutacion.Add(Mt);
                context.SaveChanges();
                return StatusCode(403, "Status 403 - Sin Mutación");
            }
        }
    }
}
