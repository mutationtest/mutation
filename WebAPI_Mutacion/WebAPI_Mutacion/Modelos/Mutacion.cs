﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI_Mutacion.Modelos
{
    public class Mutacion
    {
        [Key]
        public int IdMutaciones { get; set; }
        public string CadenaDNA { get; set; }
        public bool TieneMutacion { get; set; }
    }
}
